import React from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useState } from 'react';
import { useEffect } from 'react';
import { StyleSheet, Text, View,Image,
        TouchableOpacity, Pressable, ScrollView } from 'react-native';
import {InputField} from '../components';

const onPressLogIn = () => {
    console.log("Poner Direccion de la pantalla Login")
}

const onPressSearch = () => {
    console.log("Buscardor") 
     
}



const Home = () => {
    const loadUser = async () => {
        let id =1;
        try {
            const value = await AsyncStorage.getItem('ID')
            console.log(value)
            const res = await fetch('http://localhost:3000/users/' + id)
            console.log(await res.json())  
        } catch (error) {
            console.log(error)
        }
                
    }
    useEffect(()=>{
        loadUser()
        console.log('Entra')
    }, [])

    return (
        <View style={styles.whiteBlock}>
            <View style={styles}>
                <Text style={styles.tittle}>
                    Foodie
                </Text>
            </View>
            <View style={styles.inputLogin}>
                    <InputField
                        inputStyle={{
                            fontSize: 16,
                            marginLeft: 5,
                        }}
                        containerStyle={{
                            position:"absolute",
                            alignContent: "center",
                            marginTop: 90,
                            width:343,
                            height:50,
                            backgroundColor:'#F6F6F6',
                            borderRadius: 10,
                            marginHorizontal: 16,
                        }}
                        //rightIcon='magnify'
                        placeholder='Search'
                        autoCapitalize='none'
                        autoCorrect={false}
                    />
                    <TouchableOpacity onPress={onPressSearch}>        
                        <Image
                        style= {styles.search}
                        source= {require('../assets/png/Close.png')}
                        />
                    </TouchableOpacity>
            </View>
            <View>
                <View>
                    <Text style={styles.text}>Recent Restaurants</Text>
                    <View style={styles.navHorizontal}>
                        <ScrollView horizontal
                            showsHorizontalScrollIndicator={false}>
                            <View style={styles.eachOne}>
                                <TouchableOpacity onPress={onPressSearch}>        
                                    <Image
                                    style= {styles.images}
                                    source= {require('../assets/png/Close.png')}
                                    />
                                </TouchableOpacity>
                                <Pressable>
                                    <Text style={styles.foodText}>Tortas El wero</Text>
                                    <Text style={styles.foodText2}>Manzanillo,Colima</Text>
                                    <Text style={styles.foodText}>El diezmo #10</Text>
                                </Pressable>
                            </View>
                            <View style={styles.eachOne}>
                                <TouchableOpacity onPress={onPressSearch}>        
                                    <Image
                                    style= {styles.images}
                                    source= {require('../assets/png/Close.png')}
                                    />
                                </TouchableOpacity>
                                <Pressable>
                                    <Text style={styles.foodText}>Tacolandia</Text>
                                    <Text style={styles.foodText2}>Colima,Colima</Text>
                                    <Text style={styles.foodText}>El diezmo #10</Text>
                                </Pressable>
                            </View>
                            <View style={styles.eachOne}>
                                <TouchableOpacity onPress={onPressSearch}>        
                                    <Image
                                    style= {styles.images}
                                    source= {require('../assets/png/Close.png')}
                                    />
                                </TouchableOpacity>
                                <Pressable>
                                    <Text style={styles.foodText}>Hamburguesa de Res</Text>
                                    <Text style={styles.foodText2}>$250.00</Text>
                                    <Text style={styles.foodText}>N. Restaurante(Tacolandia)</Text>
                                </Pressable>
                            </View>
                        </ScrollView>
                    </View>
                </View>
                <View>
                    <Text style={styles.text}>Popular Restaurants</Text>
                    <View style={styles.navHorizontal}>
                        <ScrollView horizontal
                            showsHorizontalScrollIndicator={false}>
                            <View style={styles.eachOne}>
                                <TouchableOpacity onPress={onPressSearch}>        
                                    <Image
                                    style= {styles.images}
                                    source= {require('../assets/png/Close.png')}
                                    />
                                </TouchableOpacity>
                                <Pressable>
                                    <Text style={styles.foodText}>Tacolandia</Text>
                                    <Text style={styles.foodText2}>Colima,Colima</Text>
                                    <Text style={styles.foodText}>El diezmo #10</Text>
                                </Pressable>
                            </View>
                            <View style={styles.eachOne}>
                                <TouchableOpacity onPress={onPressSearch}>        
                                    <Image
                                    style= {styles.images}
                                    source= {require('../assets/png/Close.png')}
                                    />
                                </TouchableOpacity>
                                <Pressable>
                                    <Text style={styles.foodText}>Tortas El wero</Text>
                                    <Text style={styles.foodText2}>Manzanillo,Colima</Text>
                                    <Text style={styles.foodText}>El diezmo #10</Text>
                                </Pressable>
                            </View>
                            <View style={styles.eachOne}>
                                <TouchableOpacity onPress={onPressSearch}>        
                                    <Image
                                    style= {styles.images}
                                    source= {require('../assets/png/Close.png')}
                                    />
                                </TouchableOpacity>
                                <Pressable>
                                    <Text style={styles.foodText}>Tortas El wero</Text>
                                    <Text style={styles.foodText2}>Manzanillo,Colima</Text>
                                    <Text style={styles.foodText}>El diezmo #10</Text>
                                </Pressable>
                            </View>
                        </ScrollView>
                    </View>
                </View>
                <View>
                    
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    whiteBlock:{
        backgroundColor: '#FFFFFF',
        height: 812,
    },
    tittle:{
        textAlign: "center",
        alignSelf: "center",
        position: "absolute",
        marginTop: 32,
        //fontFamily: "inner",
        fontSize: 30,
        lineHeight: 36,
        fontWeight: "600",
        color: '#000000',
    },
    text:{
        textAlign: "center",
        alignSelf: "flex-start",
        position: "absolute",
        marginTop: 32,
        marginLeft: 16,
        //fontFamily: "inner",
        fontSize: 24,
        lineHeight: 36,
        fontWeight: "500",
        color: '#000000',
    },    
    text2:{
        textAlign: "center",
        alignSelf: "flex-start",
        position: "absolute",
        marginTop: 40,
        marginLeft: 16,
        //fontFamily: "inner",
        fontSize: 24,
        lineHeight: 36,
        fontWeight: "500",
        color: '#000000',
    },
    inputLogin:{
        flexDirection: "row",
        justifyContent:"flex-end",
    },
    search:{
        width: 25,
        height: 25,
        marginTop: 102,
        marginRight: 30,
    },
    navHorizontal:{
        marginTop: 92,
        marginHorizontal:10,
    },
    eachOne:{
        flexDirection: "column",

    },
    images:{
        alignSelf:"center",
        width: 200,
        height: 130,
        marginHorizontal: 6,
        backgroundColor: '#FF55FF',
    },
    foodText:{
        marginLeft:10,
        marginTop: 5,
        //fontFamily: "inner",
        fontSize: 15,
        lineHeight: 17,
        fontWeight: "normal",
        color: '#000000',
    },
    foodText2:{
        marginLeft:10,
        //alignSelf:"center",
        marginTop: 5,
        //fontFamily: "inner",
        fontSize: 16,
        lineHeight: 17,
        fontWeight: "bold",
        color: '#000000',
    },
    text:{
        textAlign: "center",
        alignSelf: "flex-start",
        position: "absolute",
        marginTop: 32,
        marginLeft: 16,
        //fontFamily: "inner",
        fontSize: 24,
        lineHeight: 36,
        fontWeight: "500",
        color: '#000000',
    },    
    menuFoodie:{
        marginTop: 110,
        width: 375,
        height: 83,
        backgroundColor: "#FAFAFA",
        flexWrap:"wrap",
        flexDirection: "row",
        borderTopWidth:1,
        justifyContent: "space-around",
        borderColor: "#E8E8E8",
    },
    iconMenu:{
        width: 30,
        height: 30,  
        marginTop: 18,
    },
});

export default Home;
