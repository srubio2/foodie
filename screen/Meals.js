import React from 'react';
import { useState } from 'react';
import { StyleSheet, Text, View,Image,
        TouchableOpacity, Pressable, ScrollView } from 'react-native';
import {InputField} from '../components';

const onPressLogIn = () => {
    console.log("Poner Direccion de la pantalla Login")
}

const onPressSearch = () => {
    console.log("Buscardor")
}

const MealsScreen = () => {
    return (
        <View style={styles.whiteBlock}>
            <View style={styles}>
                <Text style={styles.tittle}>
                    Foodie Meals
                </Text>
            </View>
            <View style={styles.inputLogin}>
                    <InputField
                        inputStyle={{
                            fontSize: 16,
                            marginLeft: 5,
                        }}
                        containerStyle={{
                            position:"absolute",
                            alignContent: "center",
                            marginTop: 90,
                            width:343,
                            height:50,
                            backgroundColor:'#F6F6F6',
                            borderRadius: 10,
                            marginHorizontal: 16,
                        }}
                        //rightIcon='magnify'
                        placeholder='Search'
                        autoCapitalize='none'
                        autoCorrect={false}
                    />
                    <TouchableOpacity onPress={onPressSearch}>        
                        <Image
                        style= {styles.search}
                        source= {require('../assets/png/Close.png')}
                        />
                    </TouchableOpacity>
            </View>
            <View>
                <View>
                    <Text style={styles.text}>Recent Meals</Text>
                    <View style={styles.navHorizontal}>
                        <ScrollView horizontal
                            showsHorizontalScrollIndicator={false}>
                            <View style={styles.eachOne}>
                                <TouchableOpacity onPress={onPressSearch}>        
                                    <Image
                                    style= {styles.images}
                                    source= {require('../assets/png/Close.png')}
                                    />
                                </TouchableOpacity>
                                <Pressable>
                                    <Text style={styles.foodText}>Chillaquiles c/Res</Text>
                                    <Text style={styles.foodText2}>$85.00</Text>
                                    <Text style={styles.foodText}>Chilaqueleria</Text>
                                </Pressable>
                            </View>
                            <View style={styles.eachOne}>
                                <TouchableOpacity onPress={onPressSearch}>        
                                    <Image
                                    style= {styles.images}
                                    source= {require('../assets/png/Close.png')}
                                    />
                                </TouchableOpacity>
                                <Pressable>
                                    <Text style={styles.foodText}>Taco de birria</Text>
                                    <Text style={styles.foodText2}>$12.00</Text>
                                    <Text style={styles.foodText}>Tacolandia</Text>
                                </Pressable>
                            </View>
                            <View style={styles.eachOne}>
                                <TouchableOpacity onPress={onPressSearch}>        
                                    <Image
                                    style= {styles.images}
                                    source= {require('../assets/png/Close.png')}
                                    />
                                </TouchableOpacity>
                                <Pressable>
                                    <Text style={styles.foodText}>Sopes de costilla</Text>
                                    <Text style={styles.foodText2}>$25.00</Text>
                                    <Text style={styles.foodText}>Piedra Lisa L.3</Text>
                                </Pressable>
                            </View>
                        </ScrollView>
                    </View>
                </View>
                <View>
                    <Text style={styles.text}>Popular Meals</Text>
                    <View style={styles.navHorizontal}>
                        <ScrollView horizontal
                            showsHorizontalScrollIndicator={false}>
                            <View style={styles.eachOne}>
                                <TouchableOpacity onPress={onPressSearch}>        
                                    <Image
                                    style= {styles.images}
                                    source= {require('../assets/png/Close.png')}
                                    />
                                </TouchableOpacity>
                                <Pressable>
                                    <Text style={styles.foodText}>Hamburguesa Loca</Text>
                                    <Text style={styles.foodText2}>$105.00</Text>
                                    <Text style={styles.foodText}>Altozeno</Text>
                                </Pressable>
                            </View>
                            <View style={styles.eachOne}>
                                <TouchableOpacity onPress={onPressSearch}>        
                                    <Image
                                    style= {styles.images}
                                    source= {require('../assets/png/Close.png')}
                                    />
                                </TouchableOpacity>
                                <Pressable>
                                    <Text style={styles.foodText}>Sopes de costilla</Text>
                                    <Text style={styles.foodText2}>$25.00</Text>
                                    <Text style={styles.foodText}>Piedra Lisa L.3</Text>
                                </Pressable>
                            </View>
                            <View style={styles.eachOne}>
                                <TouchableOpacity onPress={onPressSearch}>        
                                    <Image
                                    style= {styles.images}
                                    source= {require('../assets/png/Close.png')}
                                    />
                                </TouchableOpacity>
                                <Pressable>
                                    <Text style={styles.foodText}>Sopes de costilla</Text>
                                    <Text style={styles.foodText2}>$25.00</Text>
                                    <Text style={styles.foodText}>Piedra Lisa L.3</Text>
                                </Pressable>
                            </View>
                        </ScrollView>
                    </View>
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    whiteBlock:{
        backgroundColor: '#FFFFFF',
        height: 812,
    },
    tittle:{
        textAlign: "center",
        alignSelf: "center",
        position: "absolute",
        marginTop: 32,
        //fontFamily: "inner",
        fontSize: 30,
        lineHeight: 36,
        fontWeight: "600",
        color: '#000000',
    },
    text:{
        textAlign: "center",
        alignSelf: "flex-start",
        position: "absolute",
        marginTop: 32,
        marginLeft: 16,
        //fontFamily: "inner",
        fontSize: 24,
        lineHeight: 36,
        fontWeight: "500",
        color: '#000000',
    },    
    text2:{
        textAlign: "center",
        alignSelf: "flex-start",
        position: "absolute",
        marginTop: 40,
        marginLeft: 16,
        //fontFamily: "inner",
        fontSize: 24,
        lineHeight: 36,
        fontWeight: "500",
        color: '#000000',
    },
    inputLogin:{
        flexDirection: "row",
        justifyContent:"flex-end",
    },
    search:{
        width: 25,
        height: 25,
        marginTop: 102,
        marginRight: 30,
    },
    navHorizontal:{
        marginTop: 92,
        marginHorizontal:10,
    },
    eachOne:{
        flexDirection: "column",

    },
    images:{
        alignSelf:"center",
        width: 200,
        height: 130,
        marginHorizontal: 6,
        backgroundColor: '#FF55FF',
    },
    foodText:{
        marginLeft:10,
        marginTop: 5,
        //fontFamily: "inner",
        fontSize: 15,
        lineHeight: 17,
        fontWeight: "normal",
        color: '#000000',
    },
    foodText2:{
        marginLeft:10,
        //alignSelf:"center",
        marginTop: 5,
        //fontFamily: "inner",
        fontSize: 16,
        lineHeight: 17,
        fontWeight: "bold",
        color: '#000000',
    },
    text:{
        textAlign: "center",
        alignSelf: "flex-start",
        position: "absolute",
        marginTop: 32,
        marginLeft: 16,
        //fontFamily: "inner",
        fontSize: 24,
        lineHeight: 36,
        fontWeight: "500",
        color: '#000000',
    },    
    menuFoodie:{
        marginTop: 110,
        width: 375,
        height: 83,
        backgroundColor: "#FAFAFA",
        flexWrap:"wrap",
        flexDirection: "row",
        borderTopWidth:1,
        justifyContent: "space-around",
        borderColor: "#E8E8E8",
    },
    iconMenu:{
        width: 30,
        height: 30,  
        marginTop: 18,
    },
});

export default MealsScreen;
