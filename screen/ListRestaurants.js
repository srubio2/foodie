import React from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Image, } from "react-native";

const ListRestaurants = () => {
    return (  
        <View style={[styles.container, {
            flexDirection: "column"
          }]}>
            <View style={[styles.containerRed] } >
                <Text style={[styles.containerRedTitle]}>Profile</Text>
                <TextInput style={[styles.searchBar]}>    Search</TextInput>
            </View>
            <View style={[styles.listRes]}>
                <View >
                    <TouchableOpacity style={[styles.itemRes]}>        
                        <Image style={[styles.avatar]} source= {require('../img/Restaurants/Restaurant_Näsinneula.jpg')} />
                        <View style={{
                                    flexDirection: "column"
                                    }}>
                            <View style={{ 
                                flex: 2,  
                                top: 10, 
                                width:200,
                                flexDirection: "row", }} >
                                    <Text>RestaurantName</Text>
                                    <Text style={{ flex: 2, left:100 }}>TypeOfFood</Text>
                            </View>
                            <View style={{ flex: 1, bottom: 10 }} ><Text>Address</Text></View>
                        </View>
                    </TouchableOpacity>
                </View>

            </View>
            


        </View>
        
      );
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      textAlign: "center",
      fontFamily: "Inter",
    },
    containerRed: {
        alignItems: "center",
      },
      containerRedTitle: {
        color: "#000000",
        marginTop: 60,
        fontSize: 30,
      },
      searchBar: {
          top: 80,
          backgroundColor: "#F6F6F6",
          borderRadius: 100,
          width: 343,
          height: 50,       
          color: "#BDBDBD",           
      },
      itemRes: {
        flexDirection: "row",
        borderBottomWidth: 1,
        borderColor: "#E8E8E8",
      },
      listRes: {
        top: 120,
      },
      avatar: {
        width: 50,
        height: 50,
        margin: 10,
      },
      textItem: {
          top:5,
      }
})

export default ListRestaurants;