import React from "react";
import { StyleSheet , View, Text, TouchableHighlight, Image} from 'react-native';

const onCreateRes = () => {
  console.log("ADD ACCION FOR CREATE RESTAURANT")
}

const onCreateMeal = () => {
    console.log("ADD ACCION FOR CREATE MEAL")
  }

const AlertLetsBegin = () => {
    return (
        <View style={[styles.container, {
            flexDirection: "column"
          }]}> 
          
          <View style={[styles.theAlert]}>
          <Text style={[styles.Oops] }>Let's begin!</Text>
          <Text style={[styles.message] }>Choose what you want to create.</Text>
            
          <TouchableHighlight onPress={onCreateRes} style={[styles.btn, { top: 60}]}>        
            <Text style={[styles.btnText] }><Image source={require('../img/Icons/Store.png')} /> Create Restaurant</Text>
          </TouchableHighlight>
          <TouchableHighlight onPress={onCreateMeal} style={[styles.btn, { top: 70}]}>   
          
            <Text style={[styles.btnText] }><Image source={require('../img/Icons/Knife.png')} /> Create Meal</Text>
          </TouchableHighlight>

          </View>
          
        </View>
    );
 };

 const styles = StyleSheet.create({
    container: {
      flex: 1,
      textAlign: "center",
      fontFamily: "Inter",
      backgroundColor: "#EB5757",
    },
    theAlert: {
      position: "absolute",
      width: 343,
      height: 261,
      top: 276,
      backgroundColor: "#ffffff",
      alignSelf: "center",
      borderRadius: 8,
    },
    Oops: {
      fontSize: 30,
      textAlign: "center",
      fontWeight: "bold",
      top: 32,
    },
    message: {
      fontSize: 16,
      textAlign: "center",
      color: "#666666",
      top: 45,
    },
    btn: {
      overflow: "hidden",
      borderRadius: 100,
      backgroundColor: "#EB5757",
      height: 51,
      width: 311,
      alignSelf: "center",
    },
    btnText: {
      color: "white",
      textAlign: "center",
      fontSize: 16,
      paddingTop: 0, 
      height: 50,     
    },
  });
  
  export default AlertLetsBegin;